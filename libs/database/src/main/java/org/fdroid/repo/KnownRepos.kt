package org.fdroid.repo

/**
 * A map from canonical repo URL to lower-case fingerprint of this repo.
 * When adding new repos here, please test that adding the repo still works.
 */
internal val knownRepos = mapOf(
    "https://apt.izzysoft.de/fdroid/repo" to
        "3bf0d6abfeae2f401707b6d966be743bf0eee49c2561b9ba39073711f628937a",
    "https://guardianproject.info/fdroid/repo" to
        "b7c2eefd8dac7806af67dfcd92eb18126bc08312a7f2d6f3862e46013c7a6135",
    "https://guardianproject.info/fdroid/archive" to
        "b7c2eefd8dac7806af67dfcd92eb18126bc08312a7f2d6f3862e46013c7a6135",
    "https://cdn.kde.org/android/stable-releases/fdroid/repo" to
        "13784ba6c80ff4e2181e55c56f961eed5844cea16870d3b38d58780b85e1158f",

    // aftermarket systems
    "https://fdroid-repo.calyxinstitute.org/fdroid/repo" to
        "5da90117c91b0011ae44314ccc456cdfe406fbce3bf880072fd7c0b073e20df3",
    "https://calyxos.gitlab.io/calyx-fdroid-repo/fdroid/repo" to
        "c44d58b4547de5096138cb0b34a1cc99dab3b4274412ed753fccbfc11dc1b7b6",
    "https://divestos.org/apks/official/fdroid/repo" to
        "e4be8d6abfa4d9d4feef03cdda7ff62a73fd64b75566f6dd4e5e577550be8467",
    "https://divestos.org/apks/official/fdroid/archive" to
        "e4be8d6abfa4d9d4feef03cdda7ff62a73fd64b75566f6dd4e5e577550be8467",
    "https://divestos.org/apks/unofficial/fdroid/repo" to
        "a18cdb92f40ebfbbf778a54fd12dbd74d90f1490cb9ef2cc6c7e682dd556855d",
    "https://divestos.org/apks/unofficial/fdroid/archive" to
        "a18cdb92f40ebfbbf778a54fd12dbd74d90f1490cb9ef2cc6c7e682dd556855d",
    "https://raw.githubusercontent.com/iodeOS/fdroid/master/fdroid/repo" to
        "ec43610d9acca5d2426eb2d5eb74331930014de79d3c3acbc17dfe58aa12605f",
    "https://store.nethunter.com/repo" to
        "7e418d34c3ad4f3c37d7e6b0face13332364459c862134eb099a3bda2ccf4494",

    // messengers
    "https://briarproject.org/fdroid/repo" to
        "1fb874bee7276d28ecb2c9b06e8a122ec4bcb4008161436ce474c257cbf49bd6",
    "https://cheogram.com/fdroid/repo" to
        "ccf52dc91163f16aba9e5b150d816c3697b2c97da056713bac1ec426ec7efeb2",
    "https://molly.im/fdroid/repo" to
        "3b7e93b1fe32c6e35a93d6ddfc5afbeb1239a7c6ea6af20ff33ed53cdc38b04a",
    "https://molly.im/fdroid/foss/fdroid/repo" to
        "5198daef37fc23c14d5ee32305b2af45787bd7df2034de33ad302bdb3446df74",
    "https://app.simplex.chat/fdroid/repo" to
        "9f358ff284d1f71656a2bfaf0e005deae6aa14143720e089f11ff2ddcfeb01ba",
    "https://fdroid.getsession.org/fdroid/repo" to
        "db0e5297eb65cc22d6bd93c869943bdcfcb6a07dc69a48a0dd8c7ba698ec04e6",
    "https://releases.threema.ch/fdroid/repo" to
        "5734e753899b25775d90fe85362a49866e05ac4f83c05bef5a92880d2910639e",

    // various
    "https://microg.org/fdroid/repo" to
        "9bd06727e62796c0130eb6dab39b73157451582cbd138e86c468acc395d14165",
    "https://archive.newpipe.net/fdroid/repo" to
        "e2402c78f9b97c6c89e97db914a2751fda1d02fe2039cc0897a462bdb57e7501",
    "https://www.cromite.org/fdroid/repo" to
        "49f37e74dee483dca2b991334fb5a0200787430d0b5f9a783dd5f13695e9517b",
    "https://www.collaboraoffice.com/downloads/fdroid/repo" to
        "573258c84e149b5f4d9299e7434b2b69a8410372921d4ae586ba91ec767892cc",
    "https://static.cryptomator.org/android/fdroid/repo" to
        "f7c3ec3b0d588d3cb52983e9eb1a7421c93d4339a286398e71d7b651e8d8ecdd",
    "https://mobileapp.bitwarden.com/fdroid/repo" to
        "bc54ea6fd1cd5175bcccc47c561c5726e1c3ed7e686b6db4b18bac843a3efe6c",
    "https://fdroid.libretro.com/repo" to
        "3f05b24d497515f31feab421297c79b19552c5c81186b3750b7c131ef41d733d",
)
